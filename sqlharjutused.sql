delete from municipality where name = 'Paradiisi vald';
update municipality set name = 'Paradiisi vald', population = 8900 where id = 80;
insert into municipality (id, name, population, area, county_id) values (80, 'Paradiis', 1000, 7800, 11);
select * from county;
select * from municipality;
select m.*, c.salary from municipality m inner join county c on c.id = m.county_id where m.name = 'Viljandi linn';
select * from municipality where name = 'Viljandi linn';
select * from county where name = 'VILJANDI MAAKOND';
select m.name as municipality_name, m.population as municipality_population, m.area as municipality_area, c.name as county_name from municipality m inner join county c on c.id = m.county_id;
select m.name, m.population, m.area, c.name from municipality m inner join county c on c.id = m.county_id;
select m.*, c.name from municipality m inner join county c on c.id = m.county_id;
select * from municipality inner join county on county.id = municipality.county_id;
