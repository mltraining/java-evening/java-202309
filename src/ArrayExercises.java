import java.util.Arrays;

public class ArrayExercises {
    public static void main(String[] args) {
        // Exercise 7
//        int[] numbers = new int[5];
//        numbers[0] = 1;
//        numbers[1] = 2;
//        numbers[2] = 3;
//        numbers[3] = 4;
//        numbers[4] = 5;
        int[] numbers = {1, 2, 3, 4, 5};
        System.out.println(numbers[0]);
        System.out.println(numbers[2]);
        System.out.println(numbers[numbers.length - 1]);

//        int[] i = {1, 3};
//        System.out.println(i[0] + "" + i[1]);

        // Exercise 8
        String[] cities = {"Tallinn", "Helsinki", "Madrid", "Paris"};
        System.out.println(cities[1]);
        System.out.println(Arrays.toString(cities));

        // Exercise 9
        //        Element 1: 1, 2, 3
        //        Element 2: 4, 5, 6
        //        Element 3: 7, 8, 9, 0
        double[][] myDoubles = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0}
        };
        System.out.println(Arrays.deepToString(myDoubles));


    }
}
