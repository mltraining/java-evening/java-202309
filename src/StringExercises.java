import java.text.NumberFormat;
import java.util.Locale;

public class StringExercises {
    public static void main(String[] args) {

        // Harjutus 1
        String helloWorld = "Hello, World!";
        System.out.println(helloWorld);

        String helloWorld2 = "Hello, \"World\"!";
        System.out.println(helloWorld2);

        String hawkingQuote = """
                Steven Hawking once said: "Life would be tragic if it weren't funny."
                """;
        System.out.println(hawkingQuote);

        String complexText = "Kui liita kokku sõned \"See on teksti esimene pool\" ning \"See on teksti teine pool\", siis saame tulemuseks " +
                "\"See on teksti esimene pool See on teksti teine pool\"";
        System.out.println(complexText);

        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");

        String confusion = "Kõige rohkem segadust ekitab \"-märgi kasutamine sõne sees.";
        System.out.println(confusion);
        System.out.println("¤%$");

        // C:\minu\kataloog\fail.txt;
        String filePath = "C:\\minu\\kataloog\\fail.txt";
        System.out.println(filePath);

        String beautifulSentence = """
                Eesti keele kõige ilusam lause on: "Sõida tasa üle silla!"
                """;
        System.out.println(beautifulSentence);

        System.out.println("'kolm' - kolm, 'neli' - neli, \"viis\" - viis");
        char a = '\'';

        String name1 = "Albert";
        String name2 = "Triinu";
        String name3 = "Mari";
        // Viktoriini võitjad: Albert, Triinu, Mari.
        String winners = "Viktoriini võitjad: " + name1 + ", " + name2 + ", " + name3 + ".";
        System.out.println(winners);

        String winners2 = "Viktoriini võitjad: %s, %s, %s"
                .formatted(name1, name2, name3); // dsfasfasfdasfdas

        System.out.println(winners2);

        int population = 450_000;
        System.out.println("See on number: %s".formatted(population));
        System.out.println("See on number: %d".formatted(population));

        // Exercise 2
        String tallinnPopulation = "450 000";
        String text1 = "Tallinnas elab %s inimest.".formatted(tallinnPopulation);
        System.out.println(text1);

        int populationOfTallinn = 450_000;
//        String text2 = "Tallinnas elab %d inimest".formatted(populationOfTallinn);
//        System.out.println(text2);

//        String text3 = "Tallinnas elab %,d inimest".formatted(populationOfTallinn);
//        System.out.println(text3);

//        NumberFormat nf = NumberFormat.getInstance(new Locale("en")); // enne Java 19
        NumberFormat nf = NumberFormat.getInstance(Locale.of("en")); // alates java 19
//        String text4 = "Tallinnas elab %s inimest".formatted(nf.format(populationOfTallinn));
//        System.out.println(text4);

        // Exercise 3
        String bookTitle = "Rehepapp";
        String text5 = "Raamatu \"%s\" autor on Andrus Kivirähk".formatted(bookTitle);
        System.out.println(text5);

        // Exercise 4
        String planet1 = "Merkuur"; // CTRL + D
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        String text6 = "%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi 8 planeeti"
                .formatted(planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8);
        System.out.println(text6);


//        // Stringi meetodid ehk funktsioonid
//        String hello = "Hello";
//        System.out.println(hello.concat(", world"));
//        System.out.println(hello + ", world");
//        System.out.println(hello.substring(1,3));
//        System.out.println(hello.charAt(4));
////        System.out.println(hello == "Hello"); // Nii ei tohi võrrelda
//        System.out.println(hello.equals("Hello")); // Nii on õige
//        System.out.println(hello.equalsIgnoreCase("hello"));
//        System.out.println("Rida1\nRida2");
//        System.out.println("Text1\tText2");
//        System.out.println("Text1: \"Text2\"");
//        System.out.println("""
//                Tere, "tere"!
//                """);
    }
}
