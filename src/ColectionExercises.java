import java.util.List;
import java.util.Map;
import java.util.Set;

public class ColectionExercises {

    public static void main(String[] args) {

        // Exercise 20
//        List<String> cities = new ArrayList<>();
//        cities.add("Tallinn");
//        cities.add("Tartu");
//        cities.add("Võru");
//        cities.add("Valga");
//        cities.add("Helsinki");
        List<String> cities = List.of("Tallinn", "Tartu", "Võru", "Valga", "Helsinki");
        System.out.println("Esimene linn: " + cities.get(0));
        System.out.println("Kolmas linn: " + cities.get(2));
        System.out.println("Viimane: " + cities.get(cities.size() - 1));
        // cities.size() viimase linna jaoks

        // Exercise 21
//        Set<Integer> myNumbers = new TreeSet<>();
//        myNumbers.add(2);
//        myNumbers.add(6);
//        myNumbers.add(7);
//        myNumbers.add(7);
//        myNumbers.add(7);
//        myNumbers.add(7);
//        myNumbers.add(34);
//        myNumbers.add(45);
        Set<Integer> myNumbers = Set.of(2, 6, 7, 34, 45);
        myNumbers.forEach(tmpNum -> System.out.println(tmpNum));


        // Exercise 22
//        Map<String, List<String>> countryCitiesMap = new HashMap<>();
//        String eesti = "Eesti";
//        List<String> eestiLinnad = List.of("Tallinn", "Tartu", "Valga", "Võru");
//        countryCitiesMap.put(eesti, eestiLinnad);
//        countryCitiesMap.put("Rootsi", List.of("Stockholm", "Lund", "Uppsala", "Köping"));

        Map<String, List<String>> countryCitiesMap = Map.of(
                "Eesti", List.of("Tallinn", "Tartu", "Valga", "Võru"),
                "Rootsi", List.of("Stockholm", "Lund", "Uppsala", "Köping"),
                "Soome", List.of("Helsinki", "Espoo", "Hanko", "Jämsä")
        );
        System.out.println(countryCitiesMap);

        // Riik: Eesti
        // Riik: Soome
        // Riik: Rootsi
        Set<String> countries = countryCitiesMap.keySet();
        for (String country : countries) {
            System.out.println("Riik: " + country);
            System.out.println("Linnad:");
//            System.out.println(countryCitiesMap.get(country));
            List<String> countryCities = countryCitiesMap.get(country);
            for (String city : countryCities) {
                System.out.println("\t" + city);
            }
        }
    }

//        Map<String, Set<String>> countryCitiesMap2 = Map.of(
//                "Eesti", Set.of("Tallinn", "Tartu", "Võru"),
//                "Rootsi", Set.of("Stockholm", "Uppsala"),
//                "Soome", Set.of("Helsingi", "Espoo", "Hanko")
//        );
//        Set<String> countries = countryCitiesMap2.keySet();
//        for (String country : countries) {
//            System.out.println("Riik: " + country);
//            Set<String> linnad = countryCitiesMap2.get(country);
//            System.out.println("Linnad:");
//            for (String linn : linnad) {
//                System.out.println("\t" + linn);
//            }
//        }
}
