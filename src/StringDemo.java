public class StringDemo {
    public static void main(String[] args) {
        String text1 = "Tere";
        String text2 = "Maailm";
        String text3 = text1 + text2;
        String text4 = text1.concat(text2);
        System.out.println(text3);
        System.out.println(text4);

//        String poem = "Meil aiaäärne tänavas,\nkui armas oli see";
        String poem = """
                Meil aiaäärne tänavas,
                kui armas oli see
                """;
        System.out.println(poem);

        System.out.println("Isa ütles: \"Tule siia!\"");
        System.out.println("""
                Isa ütles: "Tule siia!"
                """);
        System.out.println("\\ test\btest2");
        System.out.println("Isa ütles: 'Tule siia!'");
        char char1 = '\'';

        String a = "a";
        String b = "A";
        if (a.equalsIgnoreCase(b)) {

        }

        String token = "Bearer asdfdasfasdfdasfdasf";
        if (token.startsWith("Bearer")) {
            // asdfsdafsd
        }

        System.out.println("B".compareTo("A"));

        String personalCode = "49403136526";
        System.out.println(personalCode.substring(1, 3));
    }
}
