public class IfExecs {
    public static void main(String[] args) {
//        // --------------------------------------------
//        // ----- Ülesanne 1
//        // --------------------------------------------
//        // Mis väärtus prinditakse konsoolile?
//
//        int a = 1;
//        if (a > 1) {
//            a = 2;
//        }
//        System.out.println(a);

//        // --------------------------------------------
//        // ----- Ülesanne 2
//        // --------------------------------------------
//        // Mis väärtus prinditakse konsoolile?
//
//        short b = 2;
//        if ((b > 1) || (b - 2 == 0)) {
//            b = 22;
//        }
//        System.out.println(b);

//        // --------------------------------------------
//        // ----- Ülesanne 3
//        // --------------------------------------------
//        // Mis väärtus prinditakse konsoolile?
//
//        byte c = 33;
//        if (c > 1 && c < 10) {
//            c = 3;
//        }
//        System.out.println(c);

//        // --------------------------------------------
//        // ----- Ülesanne 4
//        // --------------------------------------------
//        // Mis väärtus prinditakse konsoolile?
//
//        boolean d = true;
//        if (!d) {
//            d = false;
//        }
//        System.out.println(d);
//
//        // --------------------------------------------
//        // ----- Ülesanne 5
//        // --------------------------------------------
//        // Mis väärtus prinditakse konsoolile?
//
//        int e = 5;
//        if (e == 1) {
//            System.out.println("In order to write about life first you must live it.");
//        } else if (e < 2) {
//            System.out.println("Live for each second without hesitation.");
//        } else if (e > 2 && e <= 3) {
//            System.out.println("Live in the sunshine, swim the sea, drink the wild air.");
//        } else if (e == 4) {
//            System.out.println("Life is short, and it is here to be lived.");
//        } else {
//            System.out.println("Be happy for this moment. This moment is your life.");
//        }

        // --------------------------------------------
        // ----- Ülesanne 6
        // --------------------------------------------
        // Mis väärtus prinditakse konsoolile?

        int f = 2;
        switch (f) {
            case 1:
                System.out.println("In order to write about life first you must live it.");
//                break;
            case 2:
                System.out.println("Live for each second without hesitation.");
//                break;
            case 3:
                System.out.println("Live in the sunshine, swim the sea, drink the wild air.");
//                break;
            case 4:
                System.out.println("Life is short, and it is here to be lived.");
//                break;
            default:
                System.out.println("Be happy for this moment. This moment is your life.");
        }

    }
}
