public class AtithmeticExpressions {
    public static void main(String[] args) {
        // Mis on avaldise väärtus?
        int a = 5;
        a = a + 5;
        a++;
        ++a;
        a--;
        --a;
        a += 11;
        a -= 12;
        a *= 2;
        a = a * 3;
        a /= 3;
        a = a / 2;
        System.out.println(a);
    }
}
