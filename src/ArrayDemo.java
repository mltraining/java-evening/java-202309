import java.util.Arrays;

public class ArrayDemo {
    public static void main(String[] args) {
        int[] myArrayOfInts = new int[3];
        myArrayOfInts[2] = 11;
        System.out.println(myArrayOfInts[2]);
        myArrayOfInts[2] = 12;
        System.out.println(myArrayOfInts[2]);
        int[] myNumbers = {4, 5, 676, 9, 3};
        System.out.println(myNumbers[2]);

        String[] names = {"Pille", "Mari", "Mati"};
        System.out.println(names[2]);
        System.out.println("Massiivi pikkus: " + names.length);
        System.out.println(names[names.length - 1]);

//        String[][] table = new String[3][2];
        String[][] table = {
                {"Nimi", "Vanus"},
                {"Mari", "23"},
                {"Malle", "67"}
        };

        System.out.println("Teise isiku vanus: " + table[2][1]);
        System.out.println(Arrays.deepToString(table));

        // Nimi     Vanus
        // Mari     23
        // Malle    67

    }
}
