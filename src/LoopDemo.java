public class LoopDemo {
    public static void main(String[] args) {

//        int number = 5;
//        int numberOfRepetitions = 0;
//        while(numberOfRepetitions < 5) {
//            System.out.println("#");
//            numberOfRepetitions++;
//        }

//        do {
//            System.out.println("#");
//            numberOfRepetitions++;
//        } while (numberOfRepetitions < 5);

//        for (int r = 0; r < number; r++) {
//            System.out.println("#" + r);
//        }

//        for (int r = 4; r >= 0; r--) {
//            System.out.println("#" + r);
//        }

        int[] m = {3, 4, 8, 2};

        // Foreach
        for (int myNumber : m) {
            System.out.println(myNumber);
        }

//        // For
//        for (int i = 0; i < m.length; i++) {
//            System.out.println(m[i]);
//        }

    }
}
