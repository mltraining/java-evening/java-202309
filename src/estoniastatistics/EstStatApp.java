package estoniastatistics;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EstStatApp {

    public static void main(String[] args) {
//        List<String> fileLines = readFile("C:/dev/java-202309/eesti_elanikud_2022.txt");
        List<String> fileLines = readFile("eesti_elanikud_2022.txt");
        List<AdministrativeUnit> adminUnits = new ArrayList<>();
        for (String fileLine : fileLines) {
            // Must maagia. Tekitatakse objekt.
            // Paneme objekti adminUnits-isse.
            adminUnits.add(new AdministrativeUnit(fileLine));
        }

//        adminUnits.sort((e1, e2) -> e1.getName().compareTo(e2.getName()));
        // Elemendid e1 ja e2 on tüüüpi AdministrativeUnit
        // Tagastab 3 väärtust
        // negatiivne --> elemendid e1 ja e2 on juba omavahel sorteeritud
        // positiivne --> elemendid e1 ja e2 on ebakorrektses järjestuse
        // 0 - elemendid e1 ja e2 on võrdsed

//        System.out.println(adminUnits.get(99));

        // Teeme scanneri
        // Küsime kasutajalt "Sisesta haldusüksuse nimi"
        // vald
        // "leitud 89 vastet"
        // ----
        // Nimi: Anija vald
        // Rahvaarv: 5435
        // Tüüp: kohalik omavalitsus / maakond
        // ---
        // Nimi: Saue vald
        // ...

        Scanner scanner = new Scanner(System.in);
        System.out.println("Eesti omavalitsused.");
        while (true) {
            System.out.println("Sisesta haldusüksuse nimi:");
            String userInput = scanner.nextLine();
            for (AdministrativeUnit adminUnit : adminUnits) {
                if (adminUnit.getName().toLowerCase().contains(userInput.toLowerCase())) {
                    System.out.println(adminUnit);
                }
            }
        }
    }

    private static List<String> readFile(String filePath) {
        try {
            Path path = Paths.get(filePath);
            return Files.readAllLines(path);
        } catch (IOException ex) {
            System.out.println("Ei saa lugeda faili: " + ex.getMessage());
            return null;
        }
    }
}
