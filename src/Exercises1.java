import java.math.BigDecimal;

public class Exercises1 {

    public static void main(String[] args) {
        double myNum = 456.785678567856785678567856785686785678543464566;
        System.out.println(myNum);
        BigDecimal myBigDecimal = new BigDecimal("456.78567856785678567856785678568678567854");
        System.out.println(myBigDecimal);
        String myText = "test";
        boolean myBool = true;
        String myChar = "a";
        char myChar2 = 'a';
        System.out.println(myBool + ", " + myChar + ", " + myText) ;
    }
}
