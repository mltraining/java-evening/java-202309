public class LoopExercises {
    public static void main(String[] args) {
        // Exercise 11
        // Write a while-loop for printing numbers 1 … 100 to
        // the console (each number on a separate line).
//        int x = 1;
//        int upperBound = 100;
//        while (x <= upperBound) {
//            System.out.println(x++);
//        }

        // Exercise 12
        // Write a for-loop for printing numbers 1 … 100 to
        // the console (each number on a separate line).
//        for (int t = 1; t <= 100; t++) {
//            System.out.println(t);
//        }

        // Exercise 13
        // Write foreach-loop for printing numbers 1 … 10 to
        // the console (each number on a separate line).
        // Variant 1
        //        int[] n = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

//        // Variant 2
//        int[] n = new int[10];
//        for (int i = 0; i < n.length; i++) {
//            n[i] = i + 1;
//        }
//
//        for (int a : n) {
//            System.out.println(a);
//        }

        // Exercise 14
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

//        for (int i = 3; i < 100; i += 3) {
//            System.out.println(i);
//        }

        // Exercise 15
        String[] bands = {"Sun", "Metsatöll", "Queen", "Metallica"};
//        System.out.println("\"" + String.join(", ", bands) + "\"");

        String bandsText = "";
        // Variant 1
//        for (int x = 0; x < bands.length; x++) {
//            bandsText = bandsText + bands[x];
//            if (x < bands.length - 1) {
//                bandsText = bandsText + ", ";
//            }
//        }
        //        System.out.println(bandsText);

        // Variant 2
//        for (int x = 0; x < bands.length; x++) {
//            bandsText = bandsText + bands[x] + ", ";
//        }
//        bandsText = bandsText.substring(0, bandsText.length() - 2);
//        System.out.println(bandsText);

        // variant 3
        for (int x = 0; x < bands.length; x++) {
            bandsText = bandsText + bands[x] + ", ";
        }
        bandsText = bandsText;
        System.out.println(bandsText + "\b\b");
        System.out.println();

        // Exercise 16
//        bandsText = "";
//        for (int x = bands.length - 1; x >= 0; x--) {
//            bandsText = bandsText + bands[x];
//            if (x > 0) {
//                bandsText = bandsText + ", ";
//            }
//        }
//        System.out.println(bandsText);

        // Exercise 19
//        for (int row = 0; row < 8; row++) {
//            for (int column = 0; column < 19; column++) {
//                if (row % 2 == 0) {
//                    if (column % 2 == 0) {
//                        System.out.print("#");
//                    } else {
//                        System.out.print("+");
//                    }
//                } else {
//                    if (column % 2 == 0) {
//                        System.out.print("+");
//                    } else {
//                        System.out.print("#");
//                    }
//                }
//            }
//            System.out.println(" rida: " + row);
//        }

        // Pattern 3
        /*  Pattern 3:
            #
            ##
            ###
            ####
            #####
            ######
         */

//        for (int row = 0; row < 6; row++) {
//            for (int column = 0; column < 6; column++) {
//                if (column <= row) {
//                    System.out.print("#");
//                }
//            }
//            System.out.println();
//        }

        // Exercise 19
        // Pattern 1
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 19; j++) {
                if (i % 2 == 0) {
                    if (j % 2 == 0) {
                        System.out.print("#");
                    } else {
                        System.out.print("+");
                    }
                } else {
                    if (j % 2 == 1) {
                        System.out.print("#");
                    } else {
                        System.out.print("+");
                    }
                }
            }
            System.out.println(" rida: " + i);
        }

        System.out.println();

        // Pattern 2
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < i; j++) {
                System.out.print("-");
            }
            System.out.println("#");
        }

        System.out.println();

        // Pattern 3
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("#");
            }
            System.out.println();
        }

        System.out.println();

        // Pattern 4
        // Variant 1
        for (int i = 0; i < 6; i++) {
            for (int j = 6 - i; j > 0; j--) {
                System.out.print("#");
            }
            System.out.println();
        }

        System.out.println();

        for (int i = 0; i < 6; i++) {
            System.out.print("#".repeat(6 - i));
            System.out.println();
        }

    }
}
