public class BooleanExpressions {
    public static void main(String[] args) {
        // Mis on avaldise väärtus?
        boolean expression01 = true;
        boolean expression02 = true && true;
        boolean expression03 = true && false;
        boolean expression04 = true || false;
        boolean expression05 = true && false || false;
        boolean expression06 = true && false || true;
        boolean expression07 = expression05 && expression06;
        System.out.println(expression07);

//        Loogiline liitmine
//        0 + 0 = 0	false OR false = false
//        1 + 0 = 1	true OR false = true
//        1 + 1 = 1	true OR true = true
//
//        Loogiline korrutamine
//        0 * 0 = 0	false AND false = false
//        1 * 0 = 0	true AND false = false
//        1 * 1 = 1	true AND true = true

    }
}