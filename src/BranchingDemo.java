public class BranchingDemo {

    public static void main(String[] args) {

        boolean someValue = false;
//        int personAge = 18; Kompilaator ignoreerib mind!
        int personAge = Integer.parseInt(args[0]);
        System.out.println("Inimese vanus: " + args[0]);
        boolean adultCheck = personAge >= 18;
        boolean age17Check = personAge == 17;
        if (adultCheck) {
            System.out.println("Inimene on täisealine, õlle müük lubatud");
        } else if (age17Check) {
            System.out.println("Inimene on peaaegu täisealine.");
        } else {
            System.out.println("Inimene on alaealine, ei saa alkohili müüa");
        }

    }
}
