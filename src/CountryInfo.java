import java.util.List;

public class CountryInfo extends Object {
    public String name;
    public List<String> cities;
//    public String[] cities;

    public CountryInfo() {

    }

    public CountryInfo(String name, List<String> cities) {
        this.name = name;
        this.cities = cities;
    }

    public void printOut() {
        System.out.println("Riik: " + name);
        System.out.println("Linnad:");
        for (String city : cities) {
            System.out.println("    " + city);
        }
    }

    @Override
    public String toString() {
        String countryInfoText =  "Riik: " + name;
        countryInfoText += "\nLinnad:";


//            for (String city : cities) {
//                System.out.println("    " + city);
//            }
//            System.out.println();
        return countryInfoText;
    }
}
