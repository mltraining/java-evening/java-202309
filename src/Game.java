import java.util.Scanner;

public class Game {

    private static void evilFunction() {
        try {
            evilFunction();
        } catch(StackOverflowError e) {
            System.out.println("Püüdsime kinni StackOverflowErrori!");
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        evilFunction();
//        System.out.println(Math.random());
        Scanner scanner = new Scanner(System.in);
//        System.out.println("Kirjuta mulle number: ");
//        int inputNumber = scanner.nextInt();
//        System.out.println("Sa kirjutasid numbri: " + inputNumber);

        int randomNumber = (int) (Math.random() * 10_000) + 1;
        int count = 0;
        do {
            System.out.println("Sisesta number vahemikus 0 - 10 000:");
            int guessedNumber = getNumber(scanner);
            count = count + 1;
            if (guessedNumber == randomNumber) {
                System.out.println("Hurraa! Sul kulus " + count + " korda, et ära arvata!");
                break;
            } else if (guessedNumber > randomNumber) {
                System.out.println("Liiga suur.");
            } else {
                System.out.println("Liiga väike.");
            }
        } while (count <= 20);
        System.out.println("Mäng läbi! Õige number: " + randomNumber);
    }

    private static int getNumber(Scanner scanner) {
        while (true) {
            try {
                String guessedNumber = scanner.nextLine();
                return Integer.parseInt(guessedNumber);
            } catch (NumberFormatException e) {
                System.out.println("Sisend pole number. Proovi uuesti.");
            } finally {
                System.out.println("See plokk käivitub igal juhul.");
            }
        }
    }

//    private static int getNumber(Scanner scanner) {
//        while (true) {
//            try {
//                int guessedNumber = scanner.nextInt();
//                return guessedNumber;
//            } catch (InputMismatchException e) {
//                System.out.println("Sisend pole number. Proovi uuesti.");
//                scanner.next();
//            }
//        }
//    }
}
