public class DecisionMakingExercises {

    public static void main(String[] args) {
        // Exercise 2
//        String city = args[0];
//        if (city.equals("Milano")) {
//            System.out.println("Ilm on soe!");
//        } else {
//            System.out.println("Ilm polegi tähtis.");
//        }

        // Exercise 3
//        args.length

        if (args.length < 2) {
            System.out.println("Pole sisendit, ei saa printida");
            return;
        }

        int grade = Integer.parseInt(args[1]);
//        if (grade == 1) {
//            System.out.println("Hinne: kasin");
//        } else if (grade == 2) {
//            System.out.println("Hinne: mitterahuldav");
//        } else if (grade == 3) {
//            System.out.println("Hinne: rahuldav");
////        } else if (args[1].equals("4")) {
//        } else if (grade == 4) {
//            System.out.println("Hinne: hea");
//        } else if (grade == 5) {
//            System.out.println("Hinne: suurepärane");
//        } else {
//            System.out.println("Viga: ei oska sisendit lugeda");
//        }

//        switch (grade) {
//            case 1:
//                System.out.println("Hinne: kasin");
//                break;
//            case 2:
//                System.out.println("Hinne: mitterahuldav");
//                break;
//            case 3:
//                System.out.println("Hinne: rahuldav");
//                break;
//            case 4:
//                System.out.println("Hinne: hea");
//                break;
//            case 5:
//                System.out.println("Hinne: suurepärane");
//                break;
//            default:
//                System.out.println("Viga: ei oska sisendit lugeda");
//        }

//        switch (grade) {
//            case 1:
//            case 2:
//                System.out.println("Mittesooritatud");
//                break;
//            case 3:
//            case 4:
//            case 5:
//                System.out.println("Sooritatud");
//                break;
//            default:
//                System.out.println("Viga: ei oska sisendit lugeda");
//        }

//        String studentGradeText = switch (grade) {
//            case 1 -> "Hinne: kasin";
//            case 2 -> "Hinne: mitterahuldav";
//            case 3 -> "Hinne: rahuldav";
//            case 4 -> "Hinne: hea";
//            case 5 -> "Hinne: suurepärane";
//            default -> "Viga: ei oska sisendit lugeda";
//        };
//        System.out.println(studentGradeText);

        int age = 99;
        String ageText = age > 100
                ? "vana"
                : "noor";

        String ageText2;
        if (age > 100) {
            ageText2 = "vana";
        } else if (age == 100) {
            ageText2 = "peaaegu vana";
        } else {
            ageText2 = "noor";
        }

        String ageText3 = age > 100
                ? "vana"
                : age == 100 ? "peaaegu vana" : "noor";

    }
}
