package bodyanalyzer;

public class BodyInfo {
    private String name;
    private double weight;
    private int height;

    public BodyInfo() {
    }

    public BodyInfo(String name, double weight, int height) {
        this.name = name;
        this.weight = weight;
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public double getBmi() {
        double heightInM = height / 100.0; // 180 / 100 -> 1 AGA 180 / 100.0 -> 1.8
        // 23.148148 -> 23.15
        // 23.148148 * 100 -> 2314.8148
        // Math.round(2314.8148) --> 2315.0
        // 2315.0 / 100 = 23.15
        double result = weight / (heightInM * heightInM);
        return Math.round(result * 100) / 100.0;
    }

    @Override
    public String toString() {
        return "Nimi: %s, pikkus: %s, kaal: %s, KMI: %s"
                .formatted(name, height, this.weight, this.getBmi());
    }
}
